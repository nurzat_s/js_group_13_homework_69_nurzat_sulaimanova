import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `<h1>Your registration is accepted, thanks!</h1>`,
  styles: [`
    h1 {
      color: red;
    }
  `]
})

export class RegistrationComponent {

}
