export class Form {
  constructor(
    public id: string,
    public firstName: string,
    public secondName: string,
    public patronymicName: string,
    public phone: number,
    public job: string,
    public gender: string,
    public size: string,
    public comment: string
  ) {}
}
