import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Form } from './form.model';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()

export class FormService {
  formUploading = new Subject<boolean>();

  constructor(private http: HttpClient) {}


  addForm(form: Form) {
    const body = {
      firstName: form.firstName,
      secondName: form.secondName,
      patronymicName: form.patronymicName,
      phone: form.phone,
      job: form.job,
      gender: form.gender,
      size: form.size,
      comments: form.comment
    };

    this.formUploading.next(true);

    return this.http.post('https://plovo-e531e-default-rtdb.firebaseio.com/forms.json', body).pipe(
      tap(() => {
        this.formUploading.next(false);
      }, () => {
        this.formUploading.next(false);
      })
    );


  }


}
