import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ValidatePhoneDirective } from './validate-phone.directive';
import { HttpClientModule } from '@angular/common/http';
import { FormService } from './shared/form.service';
import { AppRoutingModule } from './app-routing.module';
import { RegistrationComponent } from './registration.component';

@NgModule({
  declarations: [
    AppComponent,
    ValidatePhoneDirective,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [FormService],
  bootstrap: [AppComponent]
})
export class AppModule { }
