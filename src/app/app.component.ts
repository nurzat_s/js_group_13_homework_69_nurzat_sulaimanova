import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormService } from './shared/form.service';
import { Subscription } from 'rxjs';
import { Form } from './shared/form.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy{
  @ViewChild('f') userForm!: NgForm;


  isUploading = false;
  formUploadingSubscription!: Subscription;

  constructor(private formService: FormService, private router: Router, private route: ActivatedRoute) {
  }


  onSubmit() {
    console.log(this.userForm.value);
    console.log(this.userForm);
  }

  ngOnInit(): void {
    this.formUploadingSubscription = this.formService.formUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }

  saveForm() {
    const id = Math.random().toString();

    const form = new Form(
      id,
      this.userForm.value.firstName,
      this.userForm.value.secondName,
      this.userForm.value.patronymicName,
      this.userForm.value.phone,
      this.userForm.value.job,
      this.userForm.value.gender,
      this.userForm.value.size,
      this.userForm.value.comment
    );

    const next = () => {
      void this.router.navigate(['..'], {relativeTo: this.route});
    };

    this.formService.addForm(form).subscribe(next);

  }

  ngOnDestroy() {
    this.formUploadingSubscription.unsubscribe();
  }


}
